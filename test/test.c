#include<stdio.h>
#include<fcntl.h>
#include "../fdfs_common/logger.h"
#include "../fdfs_common/shared_func.h"
int main(int argc ,char **argv)
{
    char *logfile="./log/test.log";
    char *conf_filename;
    IniContext iniContext; 
    memset(&iniContext, 0, sizeof(IniContext));
    if (argc<2)
    {
	printf("argument wrong!\n");
	return -1;
    }
    int logfd=open(logfile,O_WRONLY|O_CREAT|O_APPEND,0644);
    
    conf_filename=argv[1];
    
    log_init();
    if(iniLoadFromFile(conf_filename,&iniContext)!=0)
    {
	logError("load conf file error!\n");
	return -1;
    }
    char *tracker_server=iniGetStrValue(NULL,"tracker_server",&iniContext);
    if(tracker_server==NULL)
    {
	logError("dont have tracker_server in conf file");
	return -1;
    }
    snprintf(g_log_context.log_filename,sizeof(g_log_context.log_filename),logfile); 
//    g_log_context.log_fd=open(g_log_context.log_filename, O_WRONLY | \
				O_CREAT | O_APPEND, 0644);
    printf("tracker_server=%s\n",tracker_server);

    logInfo("test1");
    
    daemon_init(true);
    g_log_context.log_fd=logfd;
    logInfo("test2");
    logCrit("test2");
 

    while(1);
    return 0;
}

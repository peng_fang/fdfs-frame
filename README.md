#Copyright
* Copyright (C) 2008 Happy Fish / YuQing
* Copyright (C) 2014 RocFang/FangPeng

#Name 
fdfs-frame is borrowed from FastDFS. It contains some useful tool functions like log system、multi-thread lock、config system and so on.

#Usage
Compile and Link your own code with fdfs_common in a Makefile, then the functions in fdfs_common can be used.
#example
in the test directory, the main code is test.c and the content of Makefile is 
```

COMPILE = $(CC) -Wall -D_FILE_OFFSET_BITS=64 -D_GNU_SOURCE  -g -O -DDEBUG_FLAG -DOS_LINUX 
#INC_PATH = -I../common -I../tracker -I/usr/local/include 
LIB_PATH =  -lpthread -ldl -rdynamic  
FDFS_STATIC_OBJS = ../fdfs_common/fdfs_global.o \
                   ../fdfs_common/logger.o\
                   ../fdfs_common/sched_thread.o\
                   ../fdfs_common/shared_func.o\
                   ../fdfs_common/pthread_func.o\
                   ../fdfs_common/ini_file_reader.o\
                   ../fdfs_common/hash.o


STATIC_OBJS =   $(FDFS_STATIC_OBJS)
ALL_OBJS = $(STATIC_OBJS) 
ALL_PRGS = test
#all: $(ALL_OBJS) $(ALL_PRGS) $(ALL_LIBS)
all: $(ALL_OBJS) $(ALL_PRGS) 
.o:
        $(COMPILE) -o $@ $<  $(STATIC_OBJS) $(LIB_PATH)
.c:
        $(COMPILE) -o $@ $<  $(STATIC_OBJS) $(LIB_PATH)
clean:
        rm -f $(ALL_OBJS) $(ALL_PRGS)
```
Simply execute the make command in the test direcotry.
